// const MongoClient = require('mongodb').MongoClient;
const {MongoClient,ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err){
        return console.log('Unable to connect to MongoDB server.');
    }
    console.log('Connected to MongoDB Server.');

    // db.collection('Todos').find({
    //     _id: new ObjectID('5a943b7ce1b748350c86b751')
    // }).toArray().then((docs) => {
    //     console.log.apply('Todos');
    //     console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('unable to fetch Todos', err);
    // });

    // db.collection('Todos').find().count().then((count) => {
    //     console.log(`Todos count ${count}`);
    // }, (err) => {
    //     console.log('unable to fetch Todos', err);
    // });
    db.collection('Users').find({
        name: 'Chris'
    }).toArray().then((users) => {
        console.log('Users');
        console.log(JSON.stringify(users,undefined,2));
    }, (err) => {
        console.log('Unable to fetch Users', err);
    });

    // db.close();
});