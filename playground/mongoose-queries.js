const {ObjectID} = require('mongodb');
const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// var id = '5a95d871a4378c1408777d62';

// if(!ObjectID.isValid(id)){
//     console.log('ID not valid');
// }

// Todo.find({
//     _id: id      
// }).then((todos) => {
//     console.log('Todos',todos);
// });

// Todo.findOne({
//     _id: id      
// }).then((todos) => {
//     console.log('Todo',todos);
// });

// Todo.findById(id).then((todo) => {
//     if(!todo){
//         return console.log('id not found');
//     }
//     console.log('Todo by ID',todos);
// }).catch((e) => console.log(e));

var newId = '5a95b4a9ddb9d69818c08d87';

User.findById(newId).then((user) => {
    if(!user){
        return console.log('User not found');
    }
    console.log(JSON.stringify(user,undefined,2));
}).catch((e) => console.log(e));