// const MongoClient = require('mongodb').MongoClient;
const {MongoClient,ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err){
        return console.log('Unable to connect to MongoDB server.');
    }
    console.log('Connected to MongoDB Server.');

    // db.collection('Todos').findOneAndUpdate({
    //     _id: new ObjectID('5a95a10e1ca8cc6a628d4fe7')
    // }, {
    //     $set:{
    //         completed:true
    //     }
    // },{
    //     returnOriginal: false
    // }).then((result) => {
    //     console.log(result);
    // })
    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID('5a945099dad6f33fd45ab23b')
    }, {
        $set:{name:'Chris'},
        $inc:{age: 1}
    },{
        returnOriginal: false
    }).then((result) => {
        console.log(result);
    });
    
});